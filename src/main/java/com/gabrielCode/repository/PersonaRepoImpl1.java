package com.gabrielCode.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.gabrielCode.DemoConsolaApplication;

//import jdk.internal.org.jline.utils.Log;
@Repository
@Qualifier("persona1")
public class PersonaRepoImpl1 implements IPersona{
	
	public static final Logger log = LoggerFactory.getLogger(PersonaRepoImpl1.class);

	@Override
	public void registrar(String pNombre) {
		log.info("se registró "+ pNombre);
	}

}
